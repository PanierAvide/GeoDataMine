--
-- Post process of SQL database
--

-- Add relation tags on nodes
CREATE INDEX planet_osm_relation_node_node_id_idx ON planet_osm_relation_node(node_id);
CREATE AGGREGATE array_agg_flat(anyarray) (sfunc = array_cat, stype = anyarray, initcond = '{}');
UPDATE planet_osm_point
SET rel_tags = a.rel_tags
FROM (
	SELECT node_id, hstore(array_agg_flat(hstore_to_array(rel_tags))) AS rel_tags
	FROM planet_osm_relation_node
	GROUP BY node_id
) a WHERE a.node_id = osm_id;

-- Add centroid for geometries
ALTER TABLE planet_osm_line ADD COLUMN centroid GEOMETRY(Point, 3857);
ALTER TABLE planet_osm_polygon ADD COLUMN centroid GEOMETRY(Point, 3857);
UPDATE planet_osm_line SET centroid = ST_Centroid(way);
UPDATE planet_osm_polygon SET centroid = ST_Centroid(way);
CREATE INDEX planet_osm_line_centroid_idx ON planet_osm_line USING GIST(centroid);
CREATE INDEX planet_osm_polygon_centroid_idx ON planet_osm_polygon USING GIST(centroid);

-- Add boundaries for API requests
CREATE EXTENSION pg_trgm;
CREATE EXTENSION unaccent;

CREATE TABLE boundary AS
SELECT
	osm_id,
	COALESCE(tags->'ref:INSEE', tags->'ref:FR:SIREN', tags->'ref:NUTS', tags->'ref') AS ref,
	CASE WHEN boundary = 'administrative' THEN 'admin_'||admin_level ELSE boundary END AS type,
	tags->'name' AS name,
	lower(unaccent(tags->'name')) AS simple_name,
	ST_SimplifyPreserveTopology(way, 10) AS way,
	ST_Centroid(way) AS center
FROM planet_osm_polygon
WHERE
	(boundary IN ('administrative', 'political') AND "ref:INSEE" IS NOT NULL)
	OR boundary = 'local_authority';


CREATE INDEX boundary_osm_id_idx ON boundary(osm_id);
CREATE INDEX boundary_way_idx ON boundary USING GIST(way);
CREATE INDEX boundary_center_idx ON boundary USING GIST(center);
CREATE INDEX boundary_ref_trgm_idx ON boundary USING GIST(ref gist_trgm_ops);
CREATE INDEX boundary_simple_name_trgm_idx ON boundary USING GIST(name gist_trgm_ops);
CREATE INDEX boundary_type_idx ON boundary(type);
