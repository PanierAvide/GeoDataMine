--METADATA={ "name:fr": "Service public", "name:en": "Service public", "theme:fr": "Services généraux", "keywords:fr": [ "mairie", "hôtel de ville", "ambassade", "service public" ], "description:fr": "Services publics issus d'OpenStreetMap (amenity=embassy|public_building|townhall ou office=diplomatic|government)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, COALESCE(t.amenity, t.office) AS type, t.tags->'name' AS name, t.tags->'operator' AS operator,
	t.wheelchair, t.tags->'opening_hours' AS opening_hours, t.tags->'phone' AS phone, t.tags->'email' AS email,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE (t.amenity IN ('embassy', 'public_building', 'townhall') OR t.office IN ('diplomatic', 'government')) AND <GEOMCOND>
