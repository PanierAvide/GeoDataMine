--METADATA={ "name:fr": "Équipement sportif", "name:en": "Sport equipment", "theme:fr": "Sport", "keywords:fr": [ "stade", "fitness", "golf", "athlétisme" ], "description:fr": "Équipements sportifs publics issus d'OpenStreetMap (leisure=stadium|sports_centre|track|pitch|horse_riding|swimming_pool|fitness_station|golf_course ou landuse=recreation_ground, avec access!=private)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, COALESCE(t.landuse, t.leisure) AS type, t.tags->'name' AS name,
	t.tags->'operator' AS operator, t.tags->'sport' AS sport, t.wheelchair,
	t.tags->'opening_hours' AS opening_hours,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE
	(t.leisure IN ('stadium', 'sports_centre', 'track', 'pitch', 'horse_riding', 'swimming_pool', 'fitness_station', 'golf_course') OR t.landuse = 'recreation_ground')
	AND (t.access IS NULL OR t.access != 'private')
	AND <GEOMCOND>
